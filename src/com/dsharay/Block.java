package com.dsharay;

import java.awt.*;

public class Block {
    final int width = 39;
    final int height = 39;
    int xLocation;
    int yLocation;
    int gridX, gridY;
    Color color;

    public Block(int gridX, int gridY, Color color) {
        setRelativeLocation(gridX, gridY);
        this.color = color;
    }

    void setRelativeLocation(int gridX, int gridY){
        xLocation = ((gridX+1) * 40) - width; this.gridX = gridX;
        yLocation = ((gridY+1) * 40) - height; this.gridY = gridY;
    }

    void drawBlock(Graphics g){
        g.setColor(color);
        g.fillRect(xLocation, yLocation, width, height);
        System.out.println("Daw Block at: " + gridX + ", " + gridY);
    }
}
