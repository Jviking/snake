package com.dsharay;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameApp extends JFrame {

    public static int GAME_PANEL_WIDTH = 401;
    public static int GAME_PANEL_HEIGHT = 401;

    private int gameTick = 500;
    private int lastPressedKey = 0;

    public static void main(String[] args) {
        GameApp game = getGameApp();
        GamePanel panel = new GamePanel();
        game.setContentPane(panel);

        game.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                game.lastPressedKey = e.getKeyCode();
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        //noinspection InfiniteLoopStatement
        while (true) {
            try {
                Thread.sleep(speedUpGame(panel.getSnakeSize()));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                game.updateControl(panel);
                panel.moveSnake();
                panel.tryEatFood();
                panel.drawFood();
                panel.updateUI();
        }

    }

    private static int speedUpGame(int snakeSize) {
        return 500 - ((500 - 100) / (100 / snakeSize));
    }

    private void updateControl(GamePanel panel) {
        System.out.println(lastPressedKey);
        switch (lastPressedKey) {
            case 82 : {
                panel.setRandomBackground();
                panel.setRandomRedBlock();
            }
            break;
            case 37 :
                panel.updateSnakeVector(lastPressedKey);
                break;
            case 38 :
                panel.updateSnakeVector(lastPressedKey);
                break;
            case 39 :
                panel.updateSnakeVector(lastPressedKey);
                break;
            case 40 :
                panel.updateSnakeVector(lastPressedKey);
                break;
            case 27 : {
                System.exit(0);
            }
        }

        lastPressedKey = 0;
    }

    private static GameApp getGameApp() {
        GameApp game = new GameApp();
        game.setResizable(false);
        Insets windowInsets = Toolkit.getDefaultToolkit().getScreenInsets(game.getGraphicsConfiguration());
        game.setBounds(400,150,GAME_PANEL_WIDTH + 6, GAME_PANEL_HEIGHT+windowInsets.top-1);
        game.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        game.setVisible(true);
        return game;
    }
}
