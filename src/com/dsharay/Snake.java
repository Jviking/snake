package com.dsharay;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

public class Snake {
    public static final Color LIGHT_GREEN = new Color(0, 166, 0);
    public static final Color DARK_GREEN = new Color(0, 80, 0);

    private int vector = 38;
    private LinkedList<Block> body = new LinkedList<>();
    private boolean growOnNextStep = false;

    public Snake() {
        body.add(new Block(4,4, LIGHT_GREEN));
        body.add(new Block(4,5, DARK_GREEN));
    }

    public void setVector(int vector) {
        if ( !(this.vector == 37 && vector == 39)
                && !(this.vector == 38 && vector == 40)
                && !(this.vector == 39 && vector == 37)
                && !(this.vector == 40 && vector == 38)
                )
        this.vector = vector;
    }

    public List<Block> getBody() {
        return body;
    }

    public void move() {
        int newX = body.getFirst().gridX;
        int newY = body.getFirst().gridY;
        switch (vector) {
            case 37: {newX-=1; break;}
            case 38: {newY-=1; break;}
            case 39: {newX+=1; break;}
            case 40: {newY+=1; break;}
        }

        boolean borderCollision = detectBorderCollision(newX, newY);
        boolean bodyCollision = detectBodyCollision(newX, newY);
        if (borderCollision || bodyCollision) {
            System.exit(0);
        }
        int oldX, oldY;
        for (Block block: body) {
            oldX = block.gridX;
            oldY = block.gridY;
            block.setRelativeLocation(newX, newY);
            newX = oldX;
            newY = oldY;
        }
        if (growOnNextStep){
            body.addLast(new Block(newX, newY, DARK_GREEN));
            growOnNextStep = false;
        }
    }

    boolean detectBodyCollision(int newX, int newY) {
        for (Block block: body) {
            if (block.gridX == newX && block.gridY == newY) {
                return true;
            }
        }
        return false;
    }

    private boolean detectBorderCollision(int newX, int newY) {
        if (newX < 0 || newX > 9 || newY < 0 || newY > 9){
            return true;
        }
        return false;
    }

    public void eat() {
        growOnNextStep = true;
    }
}
