package com.dsharay;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.dsharay.GameApp.GAME_PANEL_HEIGHT;
import static com.dsharay.GameApp.GAME_PANEL_WIDTH;

public class GamePanel extends JPanel {

    private Color backgroundColor = Color.BLACK;
    private Block redBlock;
    private Snake snake = new Snake();

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawBackground(g);
        drawGrid(g);
        drawBlocks(g, snake.getBody());
        if (redBlock != null){
            drawBlocks(g, Arrays.asList(redBlock));
        }
    }

    private void drawBlocks(Graphics g, List<Block> blocks) {
        for(Block block: blocks) {
            block.drawBlock(g);
        }
    }

    private void drawBackground(Graphics g) {
        g.setColor(backgroundColor);
        g.drawRect(0,0, getWidth(), getHeight());
        g.fillRect(0,0,getWidth(),getHeight());
    }

    private void drawGrid(Graphics g) {
        for (int w = 0; w < GAME_PANEL_WIDTH; w+=40) {
            g.setColor(new Color(86,86,86));
            g.drawLine(w, 0, w, GAME_PANEL_HEIGHT-1);
        }

        for (int h = 0; h < GAME_PANEL_WIDTH; h+=40) {
            g.setColor(new Color(86,86,86));
            g.drawLine(0, h, GAME_PANEL_WIDTH-1, h);
        }

    }

    public void setRandomRedBlock() {
        Random random = new Random();
        boolean validCoordinate = false;
        int x = -1, y = -1;
        while (!validCoordinate) {
            x = random.nextInt(10);
            y = random.nextInt(10);
            validCoordinate = !snake.detectBodyCollision(x, y);
        }
        redBlock = new Block(x, y, new Color(128, 0,0));
    }

    void setRandomBackground() {
        Random random = new Random();
        this.backgroundColor = new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255));
    }

    public void drawFood() {
        if (redBlock == null){
            setRandomRedBlock();
        }
    }

    public void moveSnake() {
        snake.move();
    }

    public void updateSnakeVector(int lastPressedKey) {
        snake.setVector(lastPressedKey);
    }

    public void tryEatFood() {
        if (redBlock != null && snake.getBody().get(0).gridX == redBlock.gridX
          && snake.getBody().get(0).gridY == redBlock.gridY) {
            snake.eat();
            redBlock = null;
        }
    }

    public int getSnakeSize() {
        return snake.getBody().size();
    }
}
